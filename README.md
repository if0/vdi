## Clone vmdk hd to vdi

```
VBoxManage clonehd --format VDI server1-disk1.vmdk d:\drive.vdi
```

## Start
```
sudo sed -i.bak -s 's:archive:de2.archive:g' /etc/apt/sources.list
sudo apt update
sudo apt -y install qemu-utils
sudo rmmod nbd
sudo modprobe nbd max_part=16
sudo qemu-nbd -c /dev/nbd0 drive.vdi
sudo mount /dev/nbd0p1 /mnt
```

Mount disk to TARGET.

```
sudo rsync -avP \
    --exclude /boot/grub/grub.cfg \
    --exclude /etc/fstab \
    --exclude /etc/initramfs-tools/conf.d/resume \
    /mnt/ \
    TARGET
```

```
sudo sync
```

## Stop

```
sudo umount /dev/nbd0p1
sudo qemu-nbd -d /dev/nbd0
sudo rmmod nbd
```

## Remove vbox

```
sudo su -
cd /opt/Virtualbox..
./uninstall.sh
```
